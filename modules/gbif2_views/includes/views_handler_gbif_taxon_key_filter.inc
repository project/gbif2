<?php

/**
 * Filter handler for GBIF "taxonKey" field.
 */
class views_handler_gbif_taxon_key_filter extends views_handler_gbif_filter {

  /**
   * {@inheritdoc}
   */
  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    // Enable autocompletion on taxon key filter.
    $form['value']['#autocomplete_path'] = 'gbif2/species-autocomplete';
  }

}
