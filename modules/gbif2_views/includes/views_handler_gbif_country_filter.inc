<?php

/**
 * Filter handler for GBIF "country" field.
 */
class views_handler_gbif_country_filter extends views_handler_gbif_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  public function get_value_options() {
    include_once DRUPAL_ROOT . '/includes/locale.inc';
    $this->value_options = country_get_list();
  }

}
