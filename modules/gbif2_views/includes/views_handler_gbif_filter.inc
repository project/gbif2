<?php

/**
 * Filter handler for GBIF.
 */
class views_handler_gbif_filter extends views_handler_filter_string {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();
    $operators = ['=' => $operators['=']];

    return $operators;
  }

  /**
   * Generate filter.
   */
  public function generate() {
    if (!empty($this->value)) {
      return [$this->field => $this->value];
    }

    return [];
  }

}
