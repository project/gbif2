<?php

/**
 * Numeric filter handler for GBIF.
 */
class views_handler_gbif_filter_numeric extends views_handler_filter_numeric {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();
    $operators = ['=' => $operators['='], 'between' => $operators['between']];

    return $operators;
  }

  /**
   * Generate filter.
   */
  public function generate() {
    switch ($this->operator) {
      case '=':
        if (!empty($this->value['value'])) {
          return [$this->field => $this->value['value']];
        }
        break;

      case 'between':
        if (!empty($this->value['min']) && !empty($this->value['max'])) {
          return [$this->field => $this->value['min'] . ',' . $this->value['max']];
        }
        break;
    }

    return [];
  }

}
