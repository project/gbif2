<?php

/**
 * Handler for GBIF "basis of record" field.
 */
class views_handler_gbif_basis_of_record_field extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);

    $this->basisOfRecordValues = gbif2_basis_of_record_values();
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (isset($this->basisOfRecordValues[$value])) {
      return theme('html_tag', [
        'element' => [
          '#tag' => 'span',
          '#attributes' => [
            'title' => $this->basisOfRecordValues[$value]['description'],
          ],
          '#value' => $this->basisOfRecordValues[$value]['name'],
        ],
      ]);
    }
  }

}
