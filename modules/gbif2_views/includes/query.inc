<?php

use ResTelae\Gbif\Occurrences;

/**
 * Views query class using GBIF as the data source.
 */
class GbifOccurrenceViewsQuery extends views_plugin_query {

  /**
   * {@inheritdoc}
   */
  public function build(&$view) {
    // Filters.
    $view->build_info['query'] = [];
    foreach ($view->filter as $filter) {
      $view->build_info['query'] += $filter->generate();
    }

    // Pager.
    $view->init_pager();
    if ($this->pager->use_pager()) {
      $view->build_info['query'] += [
        'limit' => $this->pager->options['items_per_page'],
        'offset' => $this->pager->current_page * $this->pager->options['items_per_page'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute(&$view) {
    $start = microtime(TRUE);

    $occ = new Occurrences();
    $results = $occ->search($view->build_info['query']);
    foreach ($results['results'] as $result) {
      $view->result[] = (object) array_intersect_key($result, $view->field);
    }
    $view->total_rows = $results['count'];
    $this->pager->total_items = $results['count'];
    $this->pager->update_page_info();

    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * {@inheritdoc}
   */
  public function add_field($table, $field, $alias = '', $params = []) {
    $alias = $field;

    if (empty($this->fields[$field])) {
      $this->fields[$field] = [
        'field' => $field,
        'table' => $table,
        'alias' => $alias,
      ] + $params;
    }

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function add_where($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all the
    // default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->set_where_group('AND', $group);
    }

    $this->where[$group]['conditions'][] = array(
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    );
  }

}
