<?php

/**
 * Filter handler for GBIF "basis of record" field.
 */
class views_handler_gbif_filter_in_operator extends views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();
    $operators = ['in' => $operators['in']];

    return $operators;
  }

  /**
   * Generate filter.
   */
  public function generate() {
    if (isset($this->value[0])) {
      return [$this->field => $this->value[0]];
    }
    else {
      return [$this->field => $this->value];
    }

    return [];
  }

}
