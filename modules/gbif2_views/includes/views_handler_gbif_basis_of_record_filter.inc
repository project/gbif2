<?php

/**
 * Filter handler for GBIF "basis of record" field.
 */
class views_handler_gbif_basis_of_record_filter extends views_handler_gbif_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);

    $this->basisOfRecordValues = gbif2_basis_of_record_values();
  }

  /**
   * {@inheritdoc}
   */
  public function get_value_options() {
    $options = [];

    foreach ($this->basisOfRecordValues as $machine_name => $option_values) {
      $options[$machine_name] = $option_values['name'];
    }

    $this->value_options = $options;
  }

}
