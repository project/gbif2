<?php

/**
 * Integrate VBO with GBIF Views.
 */
class vbo_gbif_occurrence_handler_field_operations extends views_bulk_operations_handler_field_operations {

  /**
   * {@inheritdoc}
   */
  public function get_entity_type() {
    return 'gbif_occurrence';
  }

  /**
   * {@inheritdoc}
   */
  public function get_value($values, $field = NULL) {
    return $values->key;
  }

}
