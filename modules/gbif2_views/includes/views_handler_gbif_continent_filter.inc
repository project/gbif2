<?php

/**
 * Filter handler for GBIF "continent" field.
 */
class views_handler_gbif_continent_filter extends views_handler_gbif_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  public function get_value_options() {
    $this->value_options = gbif2_continent_values();
  }

}
