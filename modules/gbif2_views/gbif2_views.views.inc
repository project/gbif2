<?php

/**
 * @file
 * Views hook implementations for the GBIF Views module.
 */

/**
 * Implements hook_views_data_alter().
 */
function gbif2_views_views_data_alter(&$data) {
  $data['gbif_occurrence']['table']['base']['query class'] = 'gbif_occurrence_views_query';

  $data['views_entity_gbif_occurrence']['views_bulk_operations']['field']['handler'] = 'vbo_gbif_occurrence_handler_field_operations';

  // GBIF API does not support sort.
  unset($data['gbif_occurrence']['key']['sort']);

  $data['gbif_occurrence']['scientificName'] = [
    'title' => t('Scientific name'),
    'help' => t('A scientific name from the GBIF backbone. All included and synonym taxa are included in the search.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['taxonKey'] = [
    'title' => t('Taxon key'),
    'help' => t('Taxon classification key.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_taxon_key_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['kingdomKey'] = [
    'title' => t('Kingdom key'),
    'help' => t('Kingdom classification key.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['phylumKey'] = [
    'title' => t('Phylum key'),
    'help' => t('Phylum classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['classKey'] = [
    'title' => t('Class key'),
    'help' => t('Class classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['orderKey'] = [
    'title' => t('Order key'),
    'help' => t('Order classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['familyKey'] = [
    'title' => t('Family key'),
    'help' => t('Family classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['genusKey'] = [
    'title' => t('Genus key'),
    'help' => t('Genus classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['subgenusKey'] = [
    'title' => t('Subgenus key'),
    'help' => t('Subgenus classification key.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['datasetKey'] = [
    'title' => t('Dataset key'),
    'help' => t('The occurrence dataset key (uuid).'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['catalogNumber'] = [
    'title' => t('Catalog number'),
    'help' => t('An identifier of any form assigned by the source within a physical collection or digital dataset for the record which may not be unique, but should be fairly unique in combination with the institution and collection code.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['recordedBy'] = [
    'title' => t('Recorded by'),
    'help' => t('The person who recorded the occurrence.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['recordedById'] = [
    'title' => t('Recorded by ID'),
    'help' => t('Identifier (e.g. ORCID) for the person who recorded the occurrence.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['identifiedByID'] = [
    'title' => t('Identified by ID'),
    'help' => t('Identifier (e.g. ORCID) for the person who provided the taxonomic identification of the occurrence.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['collectionCode'] = [
    'title' => t('Collection code'),
    'help' => t('An identifier of any form assigned by the source to identify th physical collection or digital dataset uniquely within the text of an institution.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['institutionCode'] = [
    'title' => t('Institution code'),
    'help' => t('An identifier of any form assigned by the source to identify the institution the record belongs to. Not guaranteed to be unique.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['country'] = [
    'title' => t('Country'),
    'help' => t('The 2-letter country code (as per <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO-3166-1</a>) of the country in which the occurrence was recorded.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_country_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['basisOfRecord'] = [
    'title' => t('Basis of record'),
    'help' => t('Basis of record, as defined <a href="https://gbif.github.io/gbif-api/apidocs/org/gbif/api/vocabulary/BasisOfRecord.html">here</a> (mutiple field).'),
    'field' => [
      'handler' => 'views_handler_gbif_basis_of_record_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_basis_of_record_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['eventDate'] = [
    'title' => t('Event date'),
    'help' => t('Occurrence date in ISO 8601 format: yyyy, yyyy-MM, yyyy-MM-dd or MM-dd.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['year'] = [
    'title' => t('Year'),
    'help' => t('The 4 digit year. A year of 98 will be interpreted as AD 98.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['month'] = [
    'title' => t('Month'),
    'help' => t('The month of the year, starting with 1 for January.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['decimalLatitude'] = [
    'title' => t('Decimal latitude'),
    'help' => t('Latitude in decimals between -90 and 90 based on WGS 84.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['decimalLongitude'] = [
    'title' => t('Decimal longitude'),
    'help' => t('Longitude in decimals between -180 and 180 based on WGS 84.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['publishingCountry'] = [
    'title' => t('Publishing country'),
    'help' => t('The 2-letter country code (as per ISO-3166-1) of the country in which the occurrence was recorded.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_country_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['elevation'] = [
    'title' => t('Elevation'),
    'help' => t('Elevation in meters above sea level.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['depth'] = [
    'title' => t('Depth'),
    'help' => t('Depth in meters relative to elevation. For example 10 meters below a lake surface with given elevation.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter_numeric',
    ],
    'argument' => [
      'views_handler_argument_numeric',
    ],
  ];

  $data['gbif_occurrence']['issues'] = [
    'title' => t('Issues'),
    'help' => t('Issues for this occurrence (multiple field).'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['recordNumber'] = [
    'title' => t('Record Number'),
    'help' => t('Number recorded by collector of the data, different from GBIF record number. <a href="http://rs.tdwg.org/dwc/terms/#recordNumber">More info</a>.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['lastInterpreted'] = [
    'title' => t('Last interpreted'),
    'help' => t('Date the record was last modified in GBIF, in ISO 8601 format.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];

  $data['gbif_occurrence']['continent'] = [
    'title' => t('Continent'),
    'help' => t('Continent. One of <code>africa</code>, <code>antartica</code>, <code>asia</code>, <code>europe</code>, <code>north_america</code> (North America includes the Caribbean and reaches down and includes Panama), <code>oceania</code>, or <code>south_america</code>.'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_gbif_continent_filter',
    ],
    'argument' => [
      'views_handler_argument_string',
    ],
  ];
}

/**
 * Implements hook_views_plugins().
 */
function gbif2_views_views_plugins() {
  return [
    'query' => [
      'gbif_occurrence_views_query' => [
        'title' => t('GBIF'),
        'help' => t('Reads from GBIF'),
        'handler' => 'GbifOccurrenceViewsQuery',
      ],
    ],
  ];
}
