<?php

/**
 * @file
 * Provides integration with GBIF.
 */

/**
 * Implements hook_menu().
 */
function gbif2_menu() {
  $items = [];

  $items['gbif2/species-autocomplete'] = array(
    'title' => 'Species autocomplete',
    'page callback' => 'gbif2_species_autocomplete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'gbif2.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_entity_info().
 */
function gbif2_entity_info() {
  return [
    'gbif_occurrence' => [
      'label' => t('GBIF occurrence'),
      'plural label' => t('GBIF occurrences'),
      'description' => t('GBIF occurrences integration'),
      'base table' => 'gbif_occurrence',
      'entity keys' => [
        'id' => 'key',
        'label' => 'scientificName',
      ],
      'module' => 'gbif2',
      'entity class' => 'Entity',
      'controller class' => 'GbifOccurrenceController',
      'fieldable' => FALSE,
      'label callback' => 'entity_class_label',
    ],
  ];
}

/**
 * Loads a GBIF entity.
 *
 * @param int $key
 *   Integer specifying the GBIF ID to load.
 *
 * @return object
 *   A fully-loaded GBIF entity.
 */
function gbif_load($key) {
  $entities = entity_load('gbif_occurrence', [$key]);
  return reset($entities);
}

/**
 * Returns an enumeration for all values of BasisOfRecord.
 */
function gbif2_basis_of_record_values() {
  return [
    'FOSSIL_SPECIMEN' => [
      'name' => t('Fossil specimen'),
      'description' => t('An occurrence record describing a fossilized specimen.'),
    ],
    'HUMAN_OBSERVATION' => [
      'name' => t('Human observation'),
      'description' => t('An occurrence record describing an observation made by one or more people.'),
    ],
    'LITERATURE' => [
      'name' => t('Literature'),
      'description' => t('An occurrence record based on literature alone.'),
    ],
    'LIVING_SPECIMEN' => [
      'name' => t('Living specimen'),
      'description' => t('An occurrence record describing a living specimen, e.g. managed animals in a zoo or cultivated plants in a garden.'),
    ],
    'MACHINE_OBSERVATION' => [
      'name' => t('Machine observation'),
      'description' => t('An occurrence record describing an observation made by a machine.'),
    ],
    'MATERIAL_SAMPLE' => [
      'name' => t('Material sample'),
      'description' => t('An occurrence record based on samples taken from other specimens or the environment.'),
    ],
    'OBSERVATION' => [
      'name' => t('Observation'),
      'description' => t('An occurrence record describing an observation.'),
    ],
    'PRESERVED_SPECIMEN' => [
      'name' => t('Preserved specimen'),
      'description' => t('An occurrence record describing a preserved specimen.'),
    ],
    'UNKNOWN' => [
      'name' => t('Unknown'),
      'description' => t('Unknown basis for the record.'),
    ],
  ];
}

/**
 * Returns an enumeration for all values of Continent.
 */
function gbif2_continent_values() {
  return [
    'AFRICA' => t('Africa'),
    'ANTARTICA' => t('Antartica'),
    'ASIA' => t('Asia'),
    'EUROPE' => t('Europe'),
    'NORTH_AMERICA' => t('North America  (including the Caribbean and Central America)'),
    'OCEANIA' => t('Oceania'),
    'SOUTH_AMERICA' => t('South America'),
  ];
}
