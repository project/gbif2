<?php

/**
 * @file
 * Install, update and uninstall functions for the GBIF module.
 */

/**
 * Implements hook_requirements().
 */
function gbif2_requirements($phase) {
  if ($phase != 'runtime') {
    return;
  }

  $exists = class_exists('\ResTelae\Gbif\Gbif');

  return [
    'gbif2' => [
      'title' => t('GBIF library'),
      'value' => $exists ? t('Installed') : t('Missing'),
      'severity' => $exists ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    ],
  ];
}

/**
 * Implements hook_schema().
 *
 * Drupal 7 entities apparently have to rely on an SQL schema, even if we don't
 * use it. As we do not save GBIF entities locally (we only use them to query
 * remote GBIF records), this table will remain empty.
 */
function gbif2_schema() {
  return [
    'gbif_occurrence' => [
      'description' => 'Locally cached GBIF occurrences',
      'fields' => [
        'key' => [
          'description' => 'ID of GBIF occurrence',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'fields' => [
          'description' => 'Serialized fields',
          'type' => 'blob',
          'size' => 'normal',
          'not null' => TRUE,
        ],
      ],
      'primary key' => ['key'],
    ],
  ];
}
