<?php

use ResTelae\Gbif\GbifException;
use ResTelae\Gbif\Occurrences;

/**
 * Override entity methods as this is not SQL-based.
 */
class GbifOccurrenceController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function load($ids = [], $conditions = []) {
    $entities = [];

    foreach ($ids as $id) {
      $cid = 'gbif_occurrence:' . $id;
      if ($cache = cache_get($cid)) {
        $entity = $cache->data;
      }
      else {
        $occ = new Occurrences();
        try {
          $values = $occ->get($id);
        }
        catch (GbifException $e) {
          watchdog('gbif2', 'Unable to get GBIF occurrence #%id. Message returned: %msg', [
            '%id' => $id,
            '%msg' => $e->getMessage(),
          ],
          WATCHDOG_ERROR);
          continue;
        }
        if ($values) {
          $entity = entity_create('gbif_occurrence', $values);
          cache_set($cid, $entity, 'cache', CACHE_TEMPORARY);
        }
      }
      if (isset($entity)) {
        $entities[$id] = $entity;
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // We do not save GBIF entities.
  }

  /**
   * {@inheritdoc}
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    // We do not delete GBIF entities.
  }

}
