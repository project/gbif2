<?php

/**
 * @file
 * User page callback file for the gbif2 module.
 */

use ResTelae\Gbif\Species;

/**
 * Menu callback: GBIF species autocompletion.
 */
function gbif2_species_autocomplete($string = '') {
  $species = new Species();
  $suggestions = $species->nameSuggest(['q' => $string]);
  $matches = [];

  foreach ($suggestions as $suggestion) {
    $matches[$suggestion['key']] = check_plain($suggestion['scientificName'] . ' (' . $suggestion['key'] . ')');
  }

  drupal_json_output($matches);
}
